package main

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/go-chi/chi/v5"
)

type Calculator struct {
	A int `json:"a"`
	B int `json:"b"`
}

type ResponseResult struct {
	Result int `json:"result"`
}

func simpleCalculator(w http.ResponseWriter, r *http.Request) {
	var integers Calculator
	err := json.NewDecoder(r.Body).Decode(&integers)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}
	result := integers.A + integers.B

	responseResult := ResponseResult{Result: result}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(responseResult); err != nil {
		http.Error(w, "Error encoding JSON", http.StatusInternalServerError)
		return
	}
}

func main() {
	router := chi.NewRouter()
	router.Route("/", func(r chi.Router) {
		r.Post("/", simpleCalculator)
	})

	err := http.ListenAndServe(":8080", router)
	if err != nil {
		log.Println(err)
	}
}
