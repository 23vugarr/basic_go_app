FROM golang:1.21.3-bullseye

WORKDIR /app

RUN go mod init service1

RUN go get github.com/go-chi/chi/v5

COPY main.go .

EXPOSE 8080

CMD [ "go", "run", "main.go" ]