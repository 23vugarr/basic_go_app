package main

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

func TestSimpleCalculator(t *testing.T) {
	payload := []byte(`{"a": 3, "b": 7}`)
	req, err := http.NewRequest("POST", "/", bytes.NewBuffer(payload))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	simpleCalculator(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("Handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	expected := `{"result":10}`
	if rr.Body.String() == expected {
		t.Errorf("Handler returned unexpected body: got %v want %v", rr.Body.String(), expected)
	}
}

func TestMain(m *testing.M) {
	go func() {
		main()
	}()

	result := m.Run()

	os.Exit(result)
}
