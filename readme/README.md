## Setting up gitlab
![Alt text](image.png)

## Created a basic calculator using api
![Alt text](image-1.png)

## Created a Dockerfile
![Alt text](image-2.png)

## Created dockerignore
![Alt text](image-3.png)

## Created a pipeline
![Alt text](image-4.png)

## Deployment happens on aws ec2
I created a VPC, Subnet, Route Table, Internet Gateway for that ec2
![Alt text](image-5.png)

## Result of deployment
![Alt text](image-6.png)

## Pipeline worked well
![Alt text](image-7.png)
